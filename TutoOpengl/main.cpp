#ifdef WIN32
#include <GL/glew.h>

#else
#define GL3_PROTOTYPES 1
#include <GL3/gl3.h>

#endif

#include "SDL.h"
#include <iostream>

#include "SceneOpenGL.h"

int main(int argc, char **argv)
{
	//Cr�ation de la sc�ne
	SceneOpenGL scene("Chapitre 3", 800, 600);

	if (!scene.initialiserFenetre())
		return -1;

	if (!scene.initGL())
		return -1;

	scene.bouclePrincipale();

	return 0;
}